# All the data used for testing was obtained from a Google Code repository,
# maintained by the authors of the program.
# https://code.google.com/archive/p/hku-idba/downloads

# It was later modified to reduce the file size generated for testing.
# (The original files generate testing data on the order of gigabytes.)

# For idba-tran
wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/hku-idba/tran-sample-data.tar.gz
# For idba-hybrid
wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/hku-idba/hybrid-sample-data.tar.gz
# For idba-ud
wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/hku-idba/lacto-genus.tar.gz
